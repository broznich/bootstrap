FROM node:10.7-slim

RUN mkdir /app
COPY . /app
WORKDIR /app

RUN npm i && npm run build-ts

EXPOSE 3000
CMD ["/bin/sh", "-c", "node /app/dist/src/server.js"]
