module.exports = {
	globals: {
		'ts-jest': {
			tsConfigFile: 'tsconfig.json'
		}
	},
	moduleFileExtensions: [
		'ts',
		'js'
	],
	transform: {
		'^.+\\.(ts|tsx)$': 'ts-jest'
	},
	testMatch: [
		'**/test/**/*.spec.(ts|js)'
	],
  testEnvironment: 'node',
  coveragePathIgnorePatterns: ['/node_modules/', '/config/', '/dist/', '/test/']
};
